#+TITLE: Doom Emacs configuration
#+AUTHOR: nsaspy
#+PROPERTY: header-args :emacs-lisp tangle: ./config.el :tangle yes :comments link
#+STARTUP: org-startup-folded: showall
#+DISABLE_SPELLCHECKER: t
#+BEGIN_SRC emacs-lisp
;; -*- lexical-binding: t -*-
#+END_SRC

#+end_src
* TODO update config [2/8]
- [ ] Reformat config keybinds are defin with the modual
- [X] add pcap-mode
- [X] Add more magit keybinds
- [ ] add desktop notifcations
- [ ] Add theme toggle
- [ ] Replace org-adgenda views with org super agenda
- [ ] Add pipenv key binds
- [ ] add org-wiki insert-new at point function


* Styling
** Theme
I like doom's outrun eletric theme
#+begin_src emacs-lisp
(setq doom-theme 'doom-outrun-electric)
#+end_src
** Line numbers
#+begin_src emacs-lisp
(setq display-line-numbers-type t)
#+end_src

* Packages
** Magit
Set keybind for pushing to remote
Pushes the current branch to the remote
(eg: local master > remote master)
#+begin_src emacs-lisp
(map! :leader
      :desc "Push Current branch to remote branch"
      "g p P" #'magit-push-current-to-pushremote)
#+end_src

Same as above but for pulling from remote
#+begin_src emacs-lisp
(map! :leader
      :desc "Pull current branch from remote"
      "g p p" #'magit-pull-from-pushremote)
#+end_src

** Org Mode
Setting org dir
#+begin_src emacs-lisp
(setq org-directory "~/Documents/Notes/org")
#+end_src
*** Org Agenda views
I prefer being able to have difrent views within org agenda, so i can see whats comming up ahead of time.
#+begin_src emacs-lisp
(map! :leader
      :desc "Switch to week view"
      "o a w" #'org-agenda-week-view)

(map! :leader
      :desc "switch to month view"
      "o a m" #'org-agenda-month-view)

(map! :leader
      :desc "switch to month view"
      "o a y" #'org-agenda-year-view)
#+end_src
*** Babel

Tangle a file
#+begin_src emacs-lisp
(map! :leader
          :desc "Tangle a file"
          "b t" #'org-babel-tangle)
#+end_src

Execute the selected source block (used for literate programming)
#+begin_src emacs-lisp
(map! :leader
      :desc "Babel execute selected source block"
      "c b" #'org-babel-execute-src-block)

#+end_src

Execute all src blocks in buffer
#+begin_src emacs-lisp
(map! :leader
      :desc "Babel execute buffer"
      "c B" #'org-babel-execute-buffer)
#+end_src

Add languages to org babel

#+begin_src emacs-lisp
(org-babel-do-load-languages
  'org-babel-load-languages
  '((emacs-lisp . t) (org . t) (nim . t) (python . t) (erlang . t)))
#+end_src

#+RESULTS:

*** Org Tempo templates
I expirment with difrent languages, org babel makes things easy.
#+begin_src emacs-lisp
(with-eval-after-load 'org
  ;; This is needed as of Org 9.2
  (require 'org-tempo)
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("py" . "src python"))
  (add-to-list 'org-structure-template-alist '("nim" . "src nim"))
  (add-to-list 'org-structure-template-alist '("erl" . "src erlang"))
  (add-to-list 'org-structure-template-alist '("ss" . "src scheme"))
  (add-to-list 'org-structure-template-alist '("cl" . "src common-lisp")))
#+end_src


*** Org Wiki
#+begin_src emacs-lisp :tangle no
(require 'org-wiki)
#+end_src

Setting the defualt wiki loaction
#+begin_src emacs-lisp
(setq org-wiki-location-list
      '(
        "~/Documents/Notes/org/org-wiki/"))
(setq org-wiki-location (car org-wiki-location-list))
#+end_src


Setting a backup location so i dont lose my wiki and hard work.
#+begin_src emacs-lisp
(setq org-wiki-backup-location "~/.backups/")
#+end_src

Close all wiki pages when swtiching root
#+begin_src emacs-lisp
(setq org-wiki-close-root-switch t)
#+end_src

Here im creating a new prefix for org-wiki.
#+begin_src emacs-lisp
(map! :leader
      (:prefix-map ("n" . "notes")
       (:prefix ("w" . "wiki")
        :desc "New Wiki Page" "n"  #'org-wiki-new
        :desc "Switch Wiki root" "S" #'org-wiki-switch-root
        :desc "Back up wiki" "B" #'org-wiki-backup-make
        :desc "open Wiki root in dired" "o" #'org-wiki-dired
        :desc "open Wiki root  with the system file editor" "O" #'org-wiki-open
        :desc "open wiki panel" "p" #'org-wiki-panel)))
#+end_src


My atempt at creating a wiki link that failed
#+begin_src emacs-lisp
;;(map!
;; :after org
;; :map org-mode-map
;; :localleader
;; :nv "w n" #'org-wki-insert-new)
#+end_src


*** TODO Org Mode config
Create a function to tangle and sync doom at the same time

** Yasnippet
Add a new template
#+begin_src emacs-lisp
(map! :leader
      :desc "Add a neew template to yasnippet"
      "a y s" #'+snippets/new)
#+end_src

Edit a template
#+begin_src emacs-lisp
(map! :leader
      :desc "Edit template"
      "a y e" #'+snippets/find)
#+end_src

** Deft
Deft is used for notes. here im setting the default directory
#+begin_src emacs-lisp
(setq deft-extenstions '("txt", "org", "md"))
(setq deft-directory "~/Documents/Notes")
#+end_src

Deft is not recursive by defualt (it will not go into sub directories)
#+begin_src emacs-lisp
(setq deft-recursive t)
#+end_src
Tell deft to use the filename as the Title of the note
#+begin_src emacs-lisp
(setq deft-use-filename-as-title t)
#+end_src

** Notifications
#+begin_src emacs-lisp
(require 'notifications)
#+end_src
** RSS (Elfeed)
#+begin_src emacs-lisp
(require 'elfeed-org)
#+end_src
Hook elfeed-org to elfeed
#+begin_src emacs-lisp
(elfeed-org)
#+end_src
Tell elfeed where to look for org mode files

#+begin_src emacs-lisp
(setq rmh-elfeed-org-files '("~/Documents/Notes/org/rss.org"))
#+end_src


** Webpaste
Webpaste allows you to paste text to pastebin like web services
#+begin_src emacs-lisp
(require 'webpaste)
#+end_src

Tell Webpaste to confirm before upload
#+begin_src emacs-lisp
(setq webpaste-paste-confirmation t)
#+end_src

Provider priority
#+begin_src emacs-lisp
(setq webpaste-provider-priority '("ix.io" "dpaste.org"
                                   "dpaste.com" "clbin.com"
                                   "0x0.st" "bpa.st"
                                   "paste.rs"))
#+end_src



Setting Keybinds
#+begin_src emacs-lisp
(map! :leader
      (:prefix-map ("n" . "notes")
       (:prefix ("p" . "webpaste")
        :desc "paste region to a paste service" "r" #'webpaste-paste-region
        :desc "paste entire buffer to paste service" "b" #'webpaste-paste-buffer)))
#+end_src

* Programming

This section would not fit well, therefor it is in its own.

** Python

I Have problems with indent getting reset, i will explicitly set it.
#+begin_src emacs-lisp
(setq python-ident-offset 4)
#+end_src

*** Pipenv

Have pipenv open a package in emacs

* Misc
** Performance
Enable explain Pause mode

Alerts you when somthing takes some time (40ms)
#+begin_src emacs-lisp
(explain-pause-mode t)
#+end_src

** Enviroment

Nim paths are broken for me, among other env vars.


Sets PATH
#+begin_src emacs-lisp
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
#+end_src
